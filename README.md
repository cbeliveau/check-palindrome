# check-palindrome
Checks strings for palindromes. Case-insensitive and supports multi-worded strings.