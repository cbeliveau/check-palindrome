﻿using System;
using System.Linq;

namespace CheckPalindrome
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter nothing to quit.");

            bool done = false;
            while (!done)
            {
                Console.Write("Enter a phrase to check for palindromes: ");
                string input = Console.ReadLine();
                if (input.ToUpper().Equals(""))
                {
                    done = true;
                }
                else if (checkPalindrome(input.ToUpper().Replace(" ", String.Empty)))
                {
                    Console.WriteLine(input + " is a palindrome.");
                }
                else
                {
                    Console.WriteLine(input + " is not a palindrome.");
                }
            }
 
            Console.WriteLine("Terminating palindrome finder...");
            Console.WriteLine("Press ENTER to quit...");
            Console.ReadLine();
        }

        static bool checkPalindrome(string toCheck)
        {
            string reversed = new string(toCheck.ToCharArray().Reverse().ToArray());

            if (reversed.Equals(toCheck))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
